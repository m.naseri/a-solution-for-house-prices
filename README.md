# A Solution for House Prices: Advanced Regression Techniques in Kaggle (Spring 2019)
- Written in Python using Scikit-learn and Pandas, Data Mining course project, Dr. Ehsan Nazerfard
- The script included handling outliers, missing data, and encoding the data features. The models which were
fitted to dataset were LASSO, Elastic Net, Kernel Ridge, Gradient Boosting regressions. The final result was an average of the results from the models. The final score from the Kaggle was approximately 0.1155.